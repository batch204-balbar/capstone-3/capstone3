const productData = [
		{
			id: "Neck Pillow",
			name: "Neck Pillow",
			description: "Made from memory high quality memory foam",
			price: 800,
			onOffer: true
		},
		{
			id: "Luggage Strap",
			name: "Luggage Strap",
			description: "Made from memory high quality synthetic fabric",
			price: 900,
			onOffer: true
		},

	]

export default productData;