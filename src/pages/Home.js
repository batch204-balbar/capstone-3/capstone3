import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "A3C Travel Shop",
		content: "Your way to travel, your essentials for travel",
		destination: "/products",
		label: "Shop now!"
	}

	return(
		<>
			<Banner dataProp={data}/>
			<Highlights />
		</>
	)
}