import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>Travel Essentials</h2>
				        </Card.Title>
				        <Card.Text>
				            Shop for your travel essentials online
				        </Card.Text>
				    </Card.Body>
				</Card>
				</Col>

				<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>Nationwide Delivery</h2>
				        </Card.Title>
				        <Card.Text>
				            Receive your items right in your doorstep
				        </Card.Text>
				    </Card.Body>
				</Card>
				</Col>
				
				<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>24/7 Customer Support</h2>
				        </Card.Title>
				        <Card.Text>
				            Our Customer service representatives are happy to assist you 24/7
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}