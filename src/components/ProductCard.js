import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// Destructuring is done in the parameter to retrieve the courseProp.
export default function ProductCard({productProp}) {

	//console.log(props);
	// console.log(productProp);

	const {_id, name, description, price} = productProp;

	return (
	    <Card className="mb-2">
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>PhP {price}</Card.Text>
	            <Button variant="primary" onClick={"Add to Cart"}>Add to Cart</Button>*/}
	            <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
	        </Card.Body>
	    </Card>
	)
}